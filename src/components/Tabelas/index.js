import React, { useState, useEffect } from 'react';

import api from '../../services/api';

import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css';

const calendario = [
    "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
]

const Tabelas = () => {

    const [lancamento, setLancamento] = useState([]);
    const [categoria, setCategoria] = useState([]);
    const totalMes = [];
    const totalCategoria = [];

    useEffect(() => {
        async function loadLancamento() {
            const resLancamento = await api.get('/lancamentos');
            setLancamento(resLancamento.data);
        }
        loadLancamento();

        async function loadCategoria() {
            const resCategoria = await api.get('/categorias');
            setCategoria(resCategoria.data);
        }
        loadCategoria();
    }, [])

    //TOTAIS
    for (let i = 0; i <= 12; i++) {
        totalMes.push(lancamento.filter(e => e.mes_lancamento === i)
            .reduce((soma, atual) => soma + atual.valor, 0))
    }

    for (let i = 1; i <= categoria.length; i++) {
        totalCategoria.push(lancamento.filter(e => e.categoria === i)
            .reduce((soma, atual) => soma + atual.valor, 0))
    }

    //COLOCANDO NOME NAS CATEGORIAS
    // categoria.map(eCat =>
    //     lancamento.map(e => (eCat.id === e.categoria) ? e.categoria = eCat.nome : "")
    // );

    //ORDENANDO 
    lancamento.sort((a, b) => a.mes_lancamento - b.mes_lancamento);

    return (

        <Container>
            <Tabs
                defaultActiveKey="geral"
                id="uncontrolled-tab-example"
                className="justify-content-center mt-2 mb-2">

                <Tab eventKey="geral" title="Lista Geral">
                    <Table striped bordered hover responsive>
                        <tbody>
                            {lancamento.map((e, i) => (
                                <tr>
                                    <td className="text-left">
                                        <h4>{e.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </h4>
                                        {e.origem}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Tab>

                <Tab eventKey="mes" title="Por Mês">
                    <Table striped bordered hover responsive>
                        <tbody>
                            {totalMes.map((e, i) => e ? (
                                <tr>
                                    <td>
                                        <h4> {e.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </h4>
                                        {calendario[i - 1]}
                                    </td>
                                </tr>
                            ) : "")}
                        </tbody>
                    </Table>
                </Tab>

                <Tab eventKey="categoria" title="Por Categoria">
                    <Table striped bordered hover responsive="sm">
                        <tbody>
                            {totalCategoria.map((e, i) => e ? (
                                <tr>
                                    <td>
                                        <h4> {e.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </h4>
                                        {categoria[i].nome}
                                    </td>
                                </tr>
                            ) : "")}
                        </tbody>
                    </Table>
                </Tab>
            </Tabs>
        </Container>
    )
}

export default Tabelas;





