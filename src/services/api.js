import axios from 'axios';

const api = axios.create({
    baseURL: 'https://desafio-it-server.herokuapp.com'
})

export default api;

