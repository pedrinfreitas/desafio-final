import React from 'react';

import Tabelas from './components/Tabelas';
import NavBar from './components/NavBar';

import 'bootstrap/dist/css/bootstrap.min.css';
import './global-styles.css';

const App = () => ( 
  <div>
    <NavBar />
    <Tabelas /> 
  </div>
);

export default App;